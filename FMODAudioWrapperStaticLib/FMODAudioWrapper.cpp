
#include "pch.h"
#include "fmod.h"
#include "fmod.hpp"
#include "fmod_errors.h"
#include "common.h"
#include "common_platform.h"
#include "FMODAudioWrapper.h"
#include <iostream>

using namespace std;
using namespace FMODAudioWrapperLib;

void AudioWrapper::InitFMOD(void* extradriverdata)
{
	result = FMOD::System_Create(&system);
	ERRCHECK(result);

	result = system->init(32, FMOD_INIT_NORMAL, extradriverdata);
	ERRCHECK(result);
}

bool AudioWrapper::GetPlayingState()
{
	bool isPlay;
	AudioWrapper::channelGroup->isPlaying(&isPlay);
	return isPlay;
}

void AudioWrapper::PlayOneShot(bool isStreamed, const char* soundName)
{
	if (isStreamed) 
	{
		result = system->createStream(Common_MediaPath(soundName), FMOD_LOOP_OFF | FMOD_3D, 0, &sound);
		ERRCHECK(result);
	}
	else 
	{
		result = system->createSound(Common_MediaPath(soundName), FMOD_LOOP_OFF | FMOD_3D, 0, &sound);
		ERRCHECK(result);
	}

	result = sound->getNumSubSounds(&nSubSounds);
	ERRCHECK(result);

	if (nSubSounds)
	{
		sound->getSubSound(0, &soundToStream);
		ERRCHECK(result);
	}
	else soundToStream = sound;

	result = system->playSound(soundToStream, 0, false, &channel);
	ERRCHECK(result);

	result = channel->getVolume(&vol);
	ERRCHECK(result);

	result = channel->getChannelGroup(&channelGroup);
	ERRCHECK(result);
}

void AudioWrapper::PlayInLoop(bool isStreamed, const char* soundName)
{
	if (isStreamed) 
	{
		result = system->createStream(Common_MediaPath(soundName), FMOD_LOOP_NORMAL | FMOD_3D, 0, &sound);
		ERRCHECK(result);
	}
	else 
	{
		result = system->createSound(Common_MediaPath(soundName), FMOD_LOOP_NORMAL | FMOD_3D, 0, &sound);
		ERRCHECK(result);
	}

	result = sound->getNumSubSounds(&nSubSounds);
	ERRCHECK(result);

	if (nSubSounds)
	{
		sound->getSubSound(0, &soundToStream);
		ERRCHECK(result);
	}
	else soundToStream = sound;

	result = system->playSound(soundToStream, 0, false, &channel);
	ERRCHECK(result);

	result = channel->getVolume(&vol);
	ERRCHECK(result);

	result = channel->getChannelGroup(&channelGroup);
	ERRCHECK(result);
}

void AudioWrapper::CenterPan()
{
	{
		result = channelGroup->setPan(0.0f);
		ERRCHECK(result);
	}

	panStatus = 1;
	isPingPongPanStart = 1;
	panOffset = 0;

	result = system->update();
	ERRCHECK(result);
}

void AudioWrapper::LeftPan()
{
	result = channelGroup->setPan(-1.0f);
	ERRCHECK(result);

	panStatus = -1;
	isPingPongPanStart = -1;
	panOffset = 0;

	result = system->update();
	ERRCHECK(result);
}

void AudioWrapper::RightPan()
{
	result = channelGroup->setPan(1.0f);
	ERRCHECK(result);

	panStatus = 1;
	isPingPongPanStart = 1;
	panOffset = 0;

	result = system->update();
	ERRCHECK(result);
}

void AudioWrapper::PingPongPan()
{
	int out, in;
	float mat[32][32];

	// Get the pan matrix
	channelGroup->getMixMatrix(0, &out, &in, 32);
	channelGroup->getMixMatrix(&mat[0][0], &out, &in, 32);

	// calculate a pan value based on front left/front right.
	float l = 1.0f - (2 * (mat[0][0] * mat[0][0]));
	float r = 1.0f + (2 * (mat[1][0] * mat[1][0]) - 1.0f);
	pan = l + r;

	if (panStatus == -1 || isPingPongPanStart == -1)
	{

		if (pan >= 1.0f)
		{
			panOffset += 0.025f;
			if (panOffset >= 0.5f)
			{
				panOffset = 0;
				panStatus = 1;
			}
		}

		isPingPongPanStart = 0;
		pan += 0.025f;
	}

	if (panStatus == 1 || isPingPongPanStart == 1)
	{
		if (pan <= -1.0f)
			{
				panOffset -= 0.025f;
				if (panOffset <= -0.5f)
				{
					panOffset = 0;
					panStatus = -1;
				}
			}

		isPingPongPanStart = 0;
		pan -= 0.025f;
	}

	result = channelGroup->setPan(pan);
	ERRCHECK(result);

	result = system->update();
	ERRCHECK(result);
}

void AudioWrapper::VolumeUp()
{
	vol += 0.25f;
	result = channelGroup->setVolume(vol);
	ERRCHECK(result);
}

void AudioWrapper::VolumeDown()
{
	vol -= 0.25f;
	if (vol <= 0)
		vol = 0;
	result = channelGroup->setVolume(vol);
	ERRCHECK(result);
}

void AudioWrapper::AddNewSound(const char* soundName)
{
	result = system->createStream(Common_MediaPath(soundName), FMOD_LOOP_NORMAL, 0, &sound);
	ERRCHECK(result);

	result = sound->getNumSubSounds(&nSubSounds);
	ERRCHECK(result);

	if (nSubSounds)
	{
		sound->getSubSound(0, &soundToStream);
		ERRCHECK(result);
	}
	else soundToStream = sound;

	result = system->playSound(soundToStream, 0, false, &channel);
	ERRCHECK(result);
}

void AudioWrapper::PlayChannels()
{
	result = channelGroup->setPaused(false);
	ERRCHECK(result);
}

void AudioWrapper::PauseChannels()
{
	result = channelGroup->setPaused(true);
	ERRCHECK(result);
}

void AudioWrapper::StopChannels()
{
	result = channelGroup->stop();
	ERRCHECK(result);
}

void AudioWrapper::CloseFMOD()
{
	result = system->close();
	ERRCHECK(result);
	result = system->release();
	ERRCHECK(result);

	Common_Close();
}
