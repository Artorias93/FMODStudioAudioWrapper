#pragma once

#include "fmod.h"
#include "fmod.hpp"
#include "fmod_errors.h"

namespace FMODAudioWrapperLib {

	class AudioWrapper 
	{
		public:
			void* extradriverdata = 0;
			FMOD_RESULT result;
			FMOD::System* system = nullptr;
			FMOD::Sound* sound, * soundToStream;
			FMOD::Channel* channel = 0;
			FMOD::ChannelGroup* channelGroup = 0;

			int nSubSounds;
			float vol;
			float pan = 0;
			int panStatus = 1;
			bool isPingPongPanStart = 1;
			float panOffset = 0;

			void InitFMOD(void*);
			bool GetPlayingState();
			void PlayOneShot(bool isStreamed, const char* soundName);
			void PlayInLoop(bool isStreamed, const char* soundName);
			void VolumeUp();
			void VolumeDown();
			void CenterPan();
			void PingPongPan();
			void LeftPan();
			void RightPan();
			void AddNewSound(const char* soundName);
			void PlayChannels();
			void PauseChannels();
			void StopChannels();
			void CloseFMOD();
	};

}