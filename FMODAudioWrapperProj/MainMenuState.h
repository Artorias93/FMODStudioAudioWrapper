#pragma once

enum class MainMenuState {
	InitialState,
	StreamModeSelection,
	SelectTrack,
	LoopSelection,
	PlayState
};