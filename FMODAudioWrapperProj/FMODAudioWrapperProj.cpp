#include "fmod.h"
#include "fmod.hpp"
#include "fmod_errors.h"
#include "FMODAudioWrapper.h"
#include "common.h"

#include <experimental/filesystem>
#include <string>
#include <iostream>
#include "MainMenuState.h"
using namespace std;
namespace fs = experimental::filesystem;

int FMOD_Main()
{
	void* extradriverdata = 0;
	MainMenuState state = MainMenuState::InitialState;
	string currentSoundName = "";
	string mediaPath = "../media/";
	list<string> mediaFiles;
	list<string> soundsInPlayState;
	bool isStreaming = 0;
	bool isPlaying = 0;
	bool isPaused = 0;
	bool isEnded = 0;
	bool isLoop = 0;
	bool isPanSelected = 0;

	FMODAudioWrapperLib::AudioWrapper fmodWrapper;

	Common_Init(&extradriverdata);

	fmodWrapper.InitFMOD(extradriverdata);

	int numFiles = 0;
	for (auto& p : fs::directory_iterator(mediaPath))
	{
		mediaFiles.push_back(p.path().generic_string().substr(p.path().generic_string().find_last_of("/\\") + 1));
		++numFiles;
	}

	do
	{
		Common_Update();

		Common_Draw("||||||||||||||||||||||||||||||||||||||||||||||||||");
		Common_Draw("==================================================");
		Common_Draw("Welcome to a new FMOD Audio Wrapper!");
		Common_Draw("This is an Alessandro Carattoli's Project!");
		Common_Draw("It was developed for the Sound Programming course of the Master GameDev - Verona 2021/22.");
		Common_Draw("==================================================");
		Common_Draw("||||||||||||||||||||||||||||||||||||||||||||||||||\n\n");

		switch (state)
		{
		case MainMenuState::InitialState:

			Common_Draw("Press %s to START", Common_BtnStr(BTN_ACTION1));
			Common_Draw("Press %s to QUIT", Common_BtnStr(BTN_QUIT));

			if (Common_BtnPress(BTN_ACTION1))
			{
				state = MainMenuState::StreamModeSelection;
				break;
			}

			break;

		case MainMenuState::StreamModeSelection:

			isStreaming = 0;
			isPlaying = 0;
			isPaused = 0;
			isEnded = 0;
			isLoop = 0;
			isPanSelected = 0;
			currentSoundName = "";

			Common_Draw("Press %s to load sounds STATICALLY", Common_BtnStr(BTN_ACTION1));
			Common_Draw("Press %s to load sounds IN STREAMING", Common_BtnStr(BTN_ACTION2));
			Common_Draw("Press %s to quit", Common_BtnStr(BTN_QUIT));

			if (Common_BtnPress(BTN_ACTION1))
			{
				isStreaming = 0;
				state = MainMenuState::LoopSelection;
				break;
			}

			if (Common_BtnPress(BTN_ACTION2))
			{
				isStreaming = 1;
				state = MainMenuState::LoopSelection;
				break;
			}

			break;

		case MainMenuState::LoopSelection:

			Common_Draw("Press %s to play sounds in ONE-SHOT mode", Common_BtnStr(BTN_ACTION1));
			Common_Draw("Press %s to play sounds in LOOP mode", Common_BtnStr(BTN_ACTION2));
			Common_Draw("Press %s to RETURN TO THE MAIN MENU", Common_BtnStr(BTN_LEFT));
			Common_Draw("Press %s to QUIT", Common_BtnStr(BTN_QUIT));

			if (Common_BtnPress(BTN_ACTION1))
			{
				isLoop = 0;
				state = MainMenuState::SelectTrack;
				break;
			}

			if (Common_BtnPress(BTN_ACTION2))
			{
				isLoop = 1;
				state = MainMenuState::SelectTrack;
				break;
			}

			if (Common_BtnPress(BTN_LEFT))
			{
				fmodWrapper.StopChannels();
				state = MainMenuState::InitialState;
				break;
			}

			break;

		case MainMenuState::SelectTrack:

			for (int i = 0; i < numFiles; i++)
			{
				auto l_front = mediaFiles.begin();
				std::advance(l_front, i);
				std::string name = *l_front;
				Common_Draw("Press %s to OPEN %s", Common_BtnStr((Common_Button)i), name.c_str());
			}

			Common_Draw("Press %s to RETURN TO THE MAIN MENU", Common_BtnStr(BTN_LEFT));
			Common_Draw("Press %s to QUIT", Common_BtnStr(BTN_QUIT));

			for (int i = 0; i < numFiles; i++)
			{
				if (Common_BtnPress((Common_Button)i))
				{
					auto l_front = mediaFiles.begin();
					std::advance(l_front, i);
					std::string oldSoundName = currentSoundName;
					currentSoundName = *l_front;
					soundsInPlayState.push_back(currentSoundName);

					if (isLoop) fmodWrapper.PlayInLoop(isStreaming, currentSoundName.c_str());
					else fmodWrapper.PlayOneShot(isStreaming, currentSoundName.c_str());

					if (isPaused) isPlaying = 0;
					else isPlaying = 1;
					state = MainMenuState::PlayState;

					break;
				}
			}

			if (Common_BtnPress(BTN_LEFT))
			{
				fmodWrapper.StopChannels();
				state = MainMenuState::InitialState;
				break;
			}

			break;

		case MainMenuState::PlayState:
			
			if (!fmodWrapper.GetPlayingState()) isEnded = 1;
			else isEnded = 0;

			if (!isEnded)
			{
				if (isPlaying) Common_Draw("Press %s to PAUSE the sounds", Common_BtnStr(BTN_ACTION1));
				else Common_Draw("Press %s to RESUME the sounds", Common_BtnStr(BTN_ACTION1));
			}

			Common_Draw("Press %s to RESTART sounds", Common_BtnStr(BTN_ACTION2));
			Common_Draw("Press %s to STOP sounds", Common_BtnStr(BTN_ACTION3));
			Common_Draw("Press %s to ADD A NEW SOUND", Common_BtnStr(BTN_ACTION4));
			Common_Draw("Press %s to SET LEFT PAN", Common_BtnStr(BTN_ACTION5));
			Common_Draw("Press %s to SET CENTER PAN", Common_BtnStr(BTN_ACTION6));
			Common_Draw("Press %s to SET RIGHT PAN", Common_BtnStr(BTN_ACTION7));
			Common_Draw("Press %s to SET PING-PONG PAN", Common_BtnStr(BTN_ACTION8));
			Common_Draw("Press %s to VOLUME UP", Common_BtnStr(BTN_UP));
			Common_Draw("Press %s to VOLUME DOWN", Common_BtnStr(BTN_DOWN));
			Common_Draw("Press %s to RETURN TO THE MAIN MENU", Common_BtnStr(BTN_LEFT));
			Common_Draw("Press %s to QUIT", Common_BtnStr(BTN_QUIT));

			if (isPanSelected)
			{
				fmodWrapper.PingPongPan();
				Common_Sleep(20.0f);
			}

			if (Common_BtnPress(BTN_ACTION1))
			{
				if (isPlaying) fmodWrapper.PauseChannels();
				else fmodWrapper.PlayChannels();

				isPlaying = !isPlaying;
				isPaused = !isPaused;
			}

			if (Common_BtnPress(BTN_ACTION2))
			{
				fmodWrapper.StopChannels();
				isEnded = 0;

				if (isLoop)
				{
					for (list<string>::iterator it = soundsInPlayState.begin(); it != soundsInPlayState.end(); ++it) fmodWrapper.PlayInLoop(isStreaming, it->c_str());
				}
				else
				{
					for (list<string>::iterator it = soundsInPlayState.begin(); it != soundsInPlayState.end(); ++it) fmodWrapper.PlayOneShot(isStreaming, it->c_str());
				}
			}

			if (Common_BtnPress(BTN_ACTION3)) fmodWrapper.StopChannels();

			if (Common_BtnPress(BTN_ACTION4))
			{
				state = MainMenuState::SelectTrack;
				break;
			}

			if (Common_BtnPress(BTN_ACTION5))
			{
				fmodWrapper.LeftPan();
				isPanSelected = 0;
			}

			if (Common_BtnPress(BTN_ACTION6))
			{
				fmodWrapper.CenterPan();
				isPanSelected = 0;
			}

			if (Common_BtnPress(BTN_ACTION7))
			{
				fmodWrapper.RightPan();
				isPanSelected = 0;
			}

			if (Common_BtnPress(BTN_ACTION8)) isPanSelected = 1;

			if (Common_BtnPress(BTN_UP)) fmodWrapper.VolumeUp();

			if (Common_BtnPress(BTN_DOWN)) fmodWrapper.VolumeDown();

 			if (Common_BtnPress(BTN_LEFT)) 
 			{
				fmodWrapper.StopChannels();
				soundsInPlayState.clear();
 				state = MainMenuState::InitialState;
 				break;
 			}

			break;

		default:

			Common_Draw("Wrong State please quit and restart.");
			Common_Draw("Press %s to quit", Common_BtnStr(BTN_QUIT));
			break;
 		}
		
	} while (!Common_BtnPress(BTN_QUIT));

	fmodWrapper.CloseFMOD();

	return 0;
}
